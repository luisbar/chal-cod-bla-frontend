import io from 'socket.io-client';
import React, { useState, useEffect } from 'react';
import { Box, Text } from 'grommet';
import { Map, TileLayer, GeoJSON } from 'react-leaflet';

import GEOJSON from 'config/geoJson.json';
import Popup from 'components/specifics/home/Popup';

const styles = {
  mapContainer: {
    height: '100%',
  },
  twitContainer: {
    position: 'absolute',
    zIndex: 1000,
    right: '1%',
    top: '5%',
  },
};

export default () => {
  const position = [7.491643, -66.1424427];
  const zoom = 7;
  const [geoJson, setGeoJson] = useState({});
  const [points, setPoints] = useState([]);

  useEffect(() => {
    setGeoJson(GEOJSON);
    setInterval(() => {
      fetch('http://30a52637.ngrok.io/data')
      .then((response) => response.json())
      .then((data) => setPoints(data))
      .catch((error) => console.log(error.message))
    }, 5000);
  }, [])

  const random = () => {
    return Math.floor((Math.random() * 255) + 1);
  }

  const getStyle = () => {
    return {
      fillColor: `rgba(${random()}, ${random()}, ${random()}, 1)`,
      weight: 2,
      opacity: 1,
      color: 'white',
      dashArray: '3',
      fillOpacity: 0.7
    };
  };
  
  const renderPoint = (point, index) => {
    return (
      <Popup
        key={index}
        position={point.coordinates.coordinates}
        twit={point.text}
      />
    );
  };
    
  const renderTwit = (point, index) => {
    return (
      <Box
        flex
        key={index}
        border='bottom'>
        <Text
          truncate>
          {point.text}
        </Text>
      </Box>
    );
  };

  return (
    <Box
      fill>
      <Map style={styles.mapContainer} center={position} zoom={zoom}>
        <GeoJSON
          data={geoJson}
          style={getStyle}
        />
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />
        {points.map(renderPoint)}
      </Map>
      <Box
        style={styles.twitContainer}
        elevation='small'
        round='medium'
        height='90%'
        width='20%'
        background='light-1'
        pad='medium'>
        <Box
          overflow='scroll'>
          {points.slice(0, 20).map(renderTwit)}
        </Box>
      </Box>
    </Box>
  );
};
