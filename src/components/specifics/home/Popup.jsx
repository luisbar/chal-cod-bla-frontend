import React from 'react';
import { Marker, Popup } from 'react-leaflet';
import { Paragraph } from 'grommet';

export default ({ position, twit }) => {
  return (
    <Marker position={[position[1], position[0]]}>
      <Popup>
        <Paragraph>
          {twit}
        </Paragraph>
      </Popup>
    </Marker>
  );
};